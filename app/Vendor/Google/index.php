<?php
	// CLIENT ID 376183442015-uuh93j8l5lbtel4u4ddvohki1eabij61.apps.googleusercontent.com 
	// EMAIL ADDRESS  376183442015-uuh93j8l5lbtel4u4ddvohki1eabij61@developer.gserviceaccount.com  
	// CLIENT SECRET  a51jg142hjm3Olbps9xlARGf
	// REDIRECT URIS  http://localhost/profileGoogleapi/
 	// JAVASCRIPT ORIGINS  http://localhost/
?>
<?php
	//start session
	session_start();

	//include google api files
	require_once 'src/Google_Client.php';
	require_once 'src/contrib/Google_Oauth2Service.php';

	function start_oauth($client_id, $client_secret, $redirect_url) {
	
		$google_client_id = $client_id;
		$google_client_secret = $client_secret;
		$google_redirect_url = $redirect_url;
		$google_developer_key = '';

		$gClient = new Google_Client();
		$gClient->setApplicationName('Login to Sanwebe.com');
		$gClient->setClientId($google_client_id);
		$gClient->setClientSecret($google_client_secret);
		$gClient->setRedirectUri($google_redirect_url);
		$gClient->setDeveloperKey($google_developer_key);

		$google_oauthV2 = new Google_Oauth2Service($gClient);

		//If user wish to log out, we just unset Session variable
		if(isset($_REQUEST['reset'])) {
			unset($_SESSION['token']);
			$gClient->revokeToken();
			header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL)); 
			//redirect user back to page
		}

		//If code is empty, redirect user to google authentication page for code.
		//Code is required to aquire Access Token from google
		//Once we have access token, assign token to session variable
		//and we can redirect user back to page and login.
		if (isset($_GET['code'])) {
			$gClient->authenticate($_GET['code']);

		    $_SESSION['token'] = $gClient->getAccessToken();
		    // header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
		    // return;
		}

		if (isset($_SESSION['token'])) { 
		    $gClient->setAccessToken($_SESSION['token']);
		}

		if ($gClient->getAccessToken()) {
			//For logged in user, get details from google using access token
			$user                 = $google_oauthV2->userinfo->get();
			$_SESSION['token']    = $gClient->getAccessToken();
			return $user;
		}
		else {
		    //For Guest user, get google login url
		    $user['authUrl'] = $gClient->createAuthUrl();
		    return $user;
		}
	}
?>