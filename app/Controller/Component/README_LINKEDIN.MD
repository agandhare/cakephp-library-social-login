STEPS to use the LinkedinLoginComponent:

#1. Get the LinkedinLoginComponent.php file and place it into your app/Controller/Component
#2. Place the 'Linkedin' folder into your app/Vendor
#3. Call the LinkedinLoginComponent from your controller class as Follows:

	class UsersController extends AppController {
		public function index() {
			$linkedInParams['API_KEY'] = 'YOUR_API_KEY';
			$linkedInParams['API_SECRET'] = 'YOUR_API_SECRET';
			$linkedInParams['REDIRECT_URI'] = 'YOUR_REDIRECT_URI';

			$linkedinData = $this->LinkedinLogin->linkedinConnect($linkedInParams);

			// this variable '$linkedinData' will contain an array for all the info required
			// for linkedin login
		}
	}