<?php
	class LinkedinLoginComponent extends Component {

		// this method connects with linkedin and gets users data
		// scope users_profile | Positions | Educations | honors and Awards
		public function linkedinConnect($linkedInParams = array()) {
			if(isset($linkedInParams['API_KEY']) && isset($linkedInParams['API_SECRET']) && 
				isset($linkedInParams['REDIRECT_URI'])) {
				// ---------------------- App Credentials ---------------------- //
					define('API_KEY', $linkedInParams['API_KEY']);
					define('API_SECRET', $linkedInParams['API_SECRET']);
					define('REDIRECT_URI', $linkedInParams['REDIRECT_URI']);
				// ------------------- // App Credentials --------------------- //

				define('SCOPE', 'r_fullprofile r_emailaddress rw_nus');
				App::import('Vendor', 'Linkedin/linkedin'); // require linkedin library from vendors folder

				$u = array();
				$u = fetch('GET', '/v1/people/~:(firstName,lastName,email-address,headline,summary,picture-url,positions,industry,educations:(id,school-name,field-of-study,start-date,end-date,degree,activities,notes),honors-awards)');

				// pr($u);
				$user = $this->getuser($u);
				$positions = $this->getuserPositions($u);
				$educations = $this->getuserEducations($u);
				$awards = $this->getuserAwards($u);

				$linkedInData['UserProfile'] = $user;
				$linkedInData['UserPositions'] = $positions;
				$linkedInData['UserEducations'] = $educations;
				$linkedInData['UserAwards'] = $awards;
			}
			else {
				$linkedInData['_invalid'] = 'Invalid Parameters Provided';
			}

			return $linkedInData;
		}

		// gets stdObj from linkedin containing data
		// return users profile he has provided to linkedin
		protected function getuser($u = array()) {
			$user = array();
			// Initialize all the values to null
			$user['email'] = $user['firstname'] = $user['lastname'] = $user['headline'] = 
			$user['industry'] = $user['profilepic'] = $user['summary'] = "";

			if(isset($u->emailAddress)) {
				$user['email'] = $u->emailAddress;
			}

			if(isset($u->firstName)) {
				$user['firstname'] = $u->firstName;
			}

			if(isset($u->lastName)) {
				$user['lastname'] = $u->lastName;
			}

			if(isset($u->headline)) {
				$user['headline'] = $u->headline;
			}

			if(isset($u->industry)) {
				$user['industry'] = $u->industry;
			}

			if(isset($u->pictureUrl)) {
				$user['profilepic'] = $u->pictureUrl;
			}

			if(isset($u->summary)) {
				$user['summary'] = $u->summary;
			}

			return $user;
		}

		// gets stdObj from linkedin containing data
		// return users Positions / Experiences which he has worked upon
		protected function getuserPositions($u = array()) {
			$position = array();
			
			$countExp = $u->positions->_total;
			if($countExp > 0) {
				$i = 0;
				foreach($u->positions->values as $w) {
					$position[$i]['company'] = $position[$i]['start_year'] = $position[$i]['start_month'] = 
					$position[$i]['end_year'] = $position[$i]['end_month'] = $position[$i]['job_title'] = $position[$i]['job_summary'] = "";

					if(isset($w->company->name)) {
						$position[$i]['company'] = $w->company->name;
					}

					if(isset($w->startDate->year)) {
						$position[$i]['start_year'] = $w->startDate->year;
					}

					if(isset($w->startDate->month)) {
						$position[$i]['start_month'] = $w->startDate->month;
					}

					// check if users position is current
					$position[$i]['is_current'] = 0;
					if($w->isCurrent == 1) {
						$position[$i]['is_current'] = 1;
					}
					// if users position is not current get his end month / yr
					else {
						if(isset($w->endDate->year)) {
							$position[$i]['end_year'] = $w->endDate->year;
						}

						if(isset($w->endDate->month)) {
							$position[$i]['end_month'] = $w->endDate->month;
						}
					}

					if(isset($w->title)) {
						$position[$i]['job_title'] = $w->title;
					}

					if(isset($w->summary)) {
						$position[$i]['job_summary'] = $w->summary;
					}
					$i++;
				}
			}

			return $position;
		}

		// gets stdObj from linkedin containing data
		// return users Educations which he has
		protected function getuserEducations($u = array()) {
			$edu = array();

			$countEdu = $u->educations->_total;
			if($countEdu > 0) {
				$i = 0;
				foreach($u->educations->values as $e) {
					$edu[$i]['degree'] = $edu[$i]['start_year'] = $edu[$i]['end_year'] = 
					$edu[$i]['field_of_study'] = $edu[$i]['school_name'] = "";


					if(isset($e->schoolName)) {
						$edu[$i]['school_name'] = $e->schoolName;
					}

					if(isset($e->degree)) {
						$edu[$i]['degree'] = $e->degree;
					}

					if(isset($e->fieldOfStudy)) {
						$edu[$i]['field_of_study'] = $e->fieldOfStudy;
					}

					if(isset($e->startDate->year)) {
						$edu[$i]['start_year'] = $e->startDate->year;
					}

					if(isset($e->startDate->year)) {
						$edu[$i]['start_year'] = $e->startDate->year;
					}
					$i++;
				}
			}

			return $edu;
		}

		// gets stdObj from linkedin containing data
		// return users Educations which he has
		protected function getuserAwards($u = array()) {
			$awards = array();

			if(isset($u->honorsAwards->_total)) {
				$countAw = $u->honorsAwards->_total;
				if($countAw > 0) {
					$i = 0;
					foreach($u->honorsAwards->values as $h) {
						$awards[$i]['issuer'] = $awards[$i]['title'] = "";
						if(isset($h->issuer)) {
							$awards[$i]['issuer'] = $h->issuer;
						}

						if(isset($h->name)) {
							$awards[$i]['title'] = $h->name;
						}
						
						$i++;
					}
				}

				return $awards;
			}
		}
	}