<?php
	class GoogleLoginComponent extends Component {

		public function startup(Controller $controller) {
			$this->controller = $controller;
		}

		public function googleConnect($googleParams = array()) {

			if( isset($googleParams['CLIENT_ID']) && isset($googleParams['API_SECRET']) && 
				isset($googleParams['REDIRECT_URI']) ) {

				App::import('Vendor', 'Google/index');
				$u = start_oauth($googleParams['CLIENT_ID'], $googleParams['API_SECRET'], $googleParams['REDIRECT_URI']);

				if(isset($u['authUrl'])) {
		    		$this->controller->redirect($u['authUrl']);
				}
				else {
					$googleData = array();
					$googleData['email'] = $u['email'];
					$googleData['firstname'] = $u['given_name'];
					$googleData['lastname'] = $u['family_name'];
					$googleData['profilepic'] = $u['picture'];
				}
			}
			else {
				$googleData['_INVALID'] = 'Please enter valid Credentials';
			}

			return $googleData;
		}
	}